﻿using System;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;

namespace TheMetaCrippler
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    public class TheMetaCrippler : BaseUnityPlugin
    {
        public static ManualLogSource Log { get; private set; }
        internal static TheMetaCrippler Instance { get; private set; }

        internal static ConfigEntry<bool> cripplingTheMeta;

        private void Awake()
        {
            Instance = this;
            Log = this.Logger;
            Logger.LogInfo("Hello, world!");

            cripplingTheMeta = this.Config.Bind<bool>("Toggles", "CrippleTheMeta", true);

            var harmoInstance = new Harmony(PluginInfos.PLUGIN_ID);
            Logger.LogInfo("Patching ParryKillsYou");
            harmoInstance.PatchAll(typeof(ParryKillsYou));
        }

        void Start()
        {
            LLBML.Utils.ModDependenciesUtils.RegisterToModMenu(this.Info);
        }
    }
}
