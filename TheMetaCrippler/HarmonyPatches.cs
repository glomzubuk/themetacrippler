﻿using System;
using HarmonyLib;
using GameplayEntities;
using LLBML.Math;

namespace TheMetaCrippler
{
    public static class ParryKillsYou
    {
        [HarmonyPatch(typeof(PlayerEntity), nameof(PlayerEntity.ActivateParry)), HarmonyPrefix]
        public static bool ActivateParry_Prefix(PlayerEntity __instance, BallEntity onBall)
        {
            if (TheMetaCrippler.cripplingTheMeta.Value)
            {

                //__instance.attackingData.hp -= (Floatf)999999;
                //__instance.GetHitByBall(__instance.entityData.heading, __instance.playerIndex, onBall);
                __instance.GetHitByExplosion((Floatf)0.01f, onBall.ballData.flyDirection);


                return false;
            }
            return true;
        }
    }
}
